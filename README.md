# Chatit Server
## Installation
For linux:<br/>
```bash
export SPRING_DATASOURCE_URL={your_database_url}
mvn package
java -jar target/Server-0.0.1-SNAPSHOT.jar
```
## Sources
An android end-to-end encrypted chatting Application <br/>
Server Side source code: https://gitlab.com/Mustafa2856/ChatitServer <br/>
Frontend source code: https://gitlab.com/Mustafa2856/Chatit
